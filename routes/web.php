<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/index','Home@index');
Route::get('/home','Home@inicio');
Route::post('/agregar', 'Home@agregarEncuesta');
Route::get('/eliminar/{id}', 'Home@eliminarEncu')->where('id_encuesta','[0-9]+')->name('eliminar');
Route::get('/pregunta','Home@pregunta');
Route::post('/agregarPre', 'Home@agregarPregunta');
Route::get('/eliminarPre/{id}', 'Home@eliminarPre')->where('id_pregunta','[0-9]+')->name('eliminarPre');
Route::get('/getDatosEnc/{id}', 'Home@getDatosEnc')->where('id_encuesta','[0-9]+')->name('getDatosEnc');

Route::post('/modificarEnc', 'Home@modificarEnc');
Route::post('/modificarPre', 'Home@modificarPre');

Route::get('/encuesta/{id}', 'Home@encuesta')->where('id_encuesta','[0-9]+')->name('encuesta');

Route::post('/loguear', 'Home@verificarUser');

Route::get('/logout', 'Home@logout');

