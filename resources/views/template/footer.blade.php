<!-- footer -->
<footer class="footer">
    <div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="footer_widget">
                        <div class="footer_logo">
                            <a href="#">
                                <img src="img/logo.png" alt="">
                            </a>
                        </div>
                        <p class="footer_text doanar"> <a class="first" href="#">+10 783 467 3789
                        </a> <br>
                        <a href="#">encuestasfast@support.com</a></p>
                        <div class="socail_links">
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-facebook-square"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <!-- <div class="footer_widget">
                        <h3 class="footer_title">
                            service
                        </h3>
                        <ul>
                            <li><a href="#">Hosting</a></li>
                            <li><a href="#">Domain</a></li>
                            <li><a href="#">Wordpress</a></li>
                            <li><a href="#">Shared Hosting</a></li>
                        </ul>
                    
                    </div> -->
                </div>
                <div class="col-xl-2 col-md-6 col-lg-2">
                    <!-- <div class="footer_widget">
                        <h3 class="footer_title">
                            Navigation
                        </h3>
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Rooms</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">News</a></li>
                        </ul>
                    </div> -->
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="footer_widget">
                        <h3 class="footer_title">
                            Newsletter
                        </h3>
                        <form action="#" class="newsletter_form">
                            <input type="text" placeholder="Enter your mail">
                            <button type="submit">Sign Up</button>
                        </form>
                        <p class="newsletter_text">Subscribe newsletter to get updates</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copy-right_text">
        <div class="container">
            <div class="footer_border"></div>
            <div class="row">
                <div class="col-xl-12">
                    <p class="copy_right text-center">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados | Esta plantilla esta creada con <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Erika</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer -->
<!-- link that opens popup -->

<!-- form itself end-->
<form action="{{url('/loguear')}}" method="POST" autocomplete="off" id="test-form" class="white-popup-block mfp-hide">
    @csrf
    <div class="popup_box ">
        <div class="popup_inner">
            <div class="logo text-center">
                <a href="#">
                    <img src="img/form-logo.png" alt="">
                </a>
            </div>
            <h3>Iniciar Sesión</h3>
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <input type="text" placeholder="Usuario" name="usuario" id="usuario">
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="password" placeholder="Contraseña" name="clave" id="clave">
                    </div>
                    <div class="col-xl-12">
                        <input type="submit" class="boxed_btn_green" value="Iniciar">
                        <!-- <a href="home" class="boxed_btn_green">Iniciar</a> -->
                    </div>
                </div>
            <p class="doen_have_acc">¿No tienes una cuenta? <a class="dont-hav-acc" href="#">Registrate</a>
            </p>
        </div>
    </div>
</form>
<!-- form itself end -->

<!-- form itself end-->
<form id="test-form2" class="white-popup-block mfp-hide">
    <div class="popup_box ">
        <div class="popup_inner">
            <div class="logo text-center">
                <a href="#">
                    <img src="img/form-logo.png" alt="">
                </a>
            </div>
            <h3>Registrate</h3>
            <form action="#">
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <input type="text" placeholder="Correo">
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="password" placeholder="Contraseña">
                    </div>
                    <div class="col-xl-12 col-md-12">
                        <input type="Password" placeholder="Confirmar contraseña">
                    </div>
                    <div class="col-xl-12">
                        <button type="submit" class="boxed_btn_green">Registrarse</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</form>
<!-- form itself end -->



<!-- JS here -->
<script src="{{asset('props/js/vendor/modernizr-3.5.0.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/owl.carousel.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/isotope.pkgd.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/ajax-form.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/waypoints.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/jquery.counterup.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/imagesloaded.pkgd.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/scrollIt.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/jquery.scrollUp.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/wow.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/nice-select.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/jquery.slicknav.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/jquery.magnific-popup.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/plugins.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/gijgo.min.js')}}" type="text/javascript"></script>

<!--contact js-->
<script src="{{asset('props/js/contact.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/jquery.ajaxchimp.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/jquery.form.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('props/js/mail-script.js')}}" type="text/javascript"></script>

<script src="{{asset('props/js/main.js')}}" type="text/javascript"></script>
</body>

</html>