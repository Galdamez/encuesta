<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>EncuestasFast</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="{{('props/img/favicon.png')}}">
    <!-- Place favicon.ico in the root directory -->


    <!-- CSS here -->
    <link href="{{asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('props/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('props/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('props/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('props/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('props/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('props/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('props/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('props/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{asset('props/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('props/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('props/css/style.css')}}">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->

    <script src="{{url('props/js/vendor/jquery-1.12.4.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    @toastr_css
</head>