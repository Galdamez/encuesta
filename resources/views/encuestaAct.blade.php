<body>
	@include('sweet::alert')
	
	@extends('navbar')
	<!-- bradcam_area_start -->
	<div class="bradcam_area breadcam_bg overlay2">
		<h3>Modificar encuesta</h3>
	</div>
	<!-- bradcam_area_end -->
	<div class="container col-sm-6 form-container">
		<h2>Modificar encuesta</h2>
		<br>
		@foreach($e as $d)
		<form action="" method="POST" autocomplete="off">
			@csrf
			<input type="hidden" name="id_encuesta" id="id_encuesta" value="{{$d->id_encuesta}}">
			<div class="form-group row">
				<div class="col-sm-11">
					<input type="text" class="form-control" name="titulo_pre" id="titulo_pre" placeholder="Titulo de encuesta" value="{{$d->titulo_en}}">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-11">
					<input type="text" class="form-control" name="descripcion_pre" id="descripcion_pre" placeholder="Descripción" value="{{$d->descripcion_en}}">
				</div>
			</div>
			<div>
				<input type="button" class="btn btn-sm btn-primary" id="btnAct" value="Modificar">
			</div>
		</form>
		@endforeach
	</div>


	<div class="faq_area">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="accordion_heading">
						<h3>Preguntas</h3>
					</div>
					@foreach($p as $pre)
					<div id="accordion">
						<div class="card">
							<div class="card-header" id="headingTwo">
								<h5 class="mb-0">
									<button class="btn btn-link collapsed" data-toggle="collapse"
									data-target="#{{$pre->id_pregunta}}" aria-expanded="false" aria-controls="collapseTwo">
									<i class="flaticon-info"></i> {{$pre->titulo_pre}}
								</button>

							</h5>
						</div>
						<div id="{{$pre->id_pregunta}}" class="collapse" aria-labelledby="headingTwo"
							data-parent="#accordion">
							<form action="" method="POST">
								@csrf
								<div class="card-body">
									<div class="col-md-11">
										<input type="hidden" name="id_preg" id="id_preg" value="{{$pre->id_pregunta}}">
										<input type="text" name="pregunta" id="pregunta" value="{{$pre->titulo_pre}}" class="form-control">
									</div>
									<br>
									<a href="#" class="btn btn-outline-danger">Eliminar</a>
									<input type="button" class="btn btn-outline-primary" id="btnAct2" value="Modificar">
								</div>
							</form>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>


<script>

		$(document).ready(function(){


			$('#btnAct').click(function() {

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				var id_encuesta = $('#id_encuesta').val();
				var titulo_pre = $('#titulo_pre').val();
				var descripcion_pre = $('#descripcion_pre').val();

				$.ajax({
					type: 'post',
					url: "{{url('modificarEnc')}}",
					data: {id_encuesta: id_encuesta,titulo_pre: titulo_pre,descripcion_pre: descripcion_pre},
					success: function(array){
						alert('Se modifico la encuesta');
					}
				});

			});



			$('#btnAct2').click(function() {

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				var id_preg = $('#id_preg').val();
				var pregunta = $('#pregunta').val();

				$.ajax({
					type: 'post',
					url: "{{url('modificarPre')}}",
					data: {pregunta: pregunta, id_preg: id_preg},
					success: function(array){
						alert('Se modifico la pregunta');
					}
				});

			});

		});		

	</script>

