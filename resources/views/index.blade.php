
<body>
    @include('sweet::alert')

    <!-- header-start -->
    @extends('navbar')
    <!-- header-end -->

    <!-- slider_area_start -->
    <div class="slider_area">
        <div class="single_slider d-flex align-items-center justify-content-center slider_bg_1 overlay2">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-9">
                        <div class="slider_text text-center">
                            <h3>EncuestaFast te permitira realizar encuestas online de forma gratuita</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider_area_end -->

    <!-- latest_new_area_start -->
    <div class="latest_new_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-100">
                        <h3>
                            En tan sólo 3 pasos
                        </h3>
                        <p>Tu puede crear tus encuestas fácil y rápido sin ninguna complicación.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="single_news">
                        <div class="thumb">
                            <a>
                                <img src="{{('props/img/news/1.png')}}" alt="">
                            </a>
                        </div>
                        <div class="news_content">
                            <h3>PASO 1. CREA TU ENCUESTA</h3>
                            <p class="news_info">Utiliza nuestro ágil sistema de edición de encuestas para redactar encuestas a tu medida.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="single_news">
                        <div class="thumb">
                            <a>
                                <img src="{{('props/img/news/2.png')}}" alt="">
                            </a>
                        </div>
                        <div class="news_content">
                            <h3>PASO 2. PUBLICALA O ENVIALA</h3>
                            <p class="news_info">Envía por correo el enlace a tus contactos, publícala en tu blog o sitio web, postéala en un foro o compártela en una red social.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 col-lg-4">
                    <div class="single_news">
                        <div class="thumb">
                            <a>
                                <img src="{{('props/img/news/3.png')}}" alt="">
                            </a>
                        </div>
                        <div class="news_content">
                            <h3>PASO 3. ANALIZA LOS RESULTADOS</h3>
                            <p class="news_info">Desde el momento en que alguien responda a tu encuesta, podrás consultar los datos obtenidos sin esperas.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- latest_new_area_end -->

    <!-- core_features_start -->
    <div class="core_features">
        <div class="container">
            <div class="border-bottm">
                <div class="row">
                    <div class="col-xl-6 col-md-6">
                        <div class="featuures_heading">
                            <h3>Por medio de las encuestas puedes saber</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                            aria-labelledby="nav-home-tab">
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="single_features">
                                        <div class="icon"><i class="flaticon-browser"></i></div>
                                        <div class="features_info">
                                            <h4>
                                                Test de Producto
                                            </h4>
                                            <p>Averigua si tu producto tiene salida en el mercado.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="single_features">
                                        <div class="icon blue"><i class="flaticon-security"></i></div>
                                        <div class="features_info">
                                            <h4>
                                                Clima Laboral
                                            </h4>
                                            <p>¿Trabajan tus empleados a gusto?</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="single_features">
                                        <div class="icon pink"><i class="flaticon-like"></i></div>
                                        <div class="features_info">
                                            <h4>
                                                Satisfacción del Cliente
                                            </h4>
                                            <p>Comprueba el grado de satisfacción de tus clientes con los productos y servicio de tu empresa.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="single_features">
                                        <div class="icon yellow"><i class="flaticon-lock"></i></div>
                                        <div class="features_info">
                                            <h4>
                                                Perfil del Visitante
                                            </h4>
                                            <p>Descubre cómo llegan los internautas a tu sitio web.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="row">
                                <div class="col-xl-6">
                                    <div class="single_features">
                                        <div class="icon"><i class="flaticon-browser"></i></div>
                                        <div class="features_info">
                                            <h4>
                                                Free Domain for 1st Year
                                            </h4>
                                            <p>Our set he for firmament morning sixth subdue darkness creeping gathered
                                                divide our let god moving.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="single_features">
                                        <div class="icon blue"><i class="flaticon-security"></i></div>
                                        <div class="features_info">
                                            <h4>
                                                Free SSL Certificate
                                            </h4>
                                            <p>Our set he for firmament morning sixth subdue darkness creeping gathered
                                                divide our let god moving.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="single_features">
                                        <div class="icon pink"><i class="flaticon-like"></i></div>
                                        <div class="features_info">
                                            <h4>
                                                30-Day Money-Back Guarantee
                                            </h4>
                                            <p>Our set he for firmament morning sixth subdue darkness creeping gathered
                                                divide our let god moving.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="single_features">
                                        <div class="icon yellow"><i class="flaticon-lock"></i></div>
                                        <div class="features_info">
                                            <h4>
                                                Spam Protection
                                            </h4>
                                            <p>Our set he for firmament morning sixth subdue darkness creeping gathered
                                                divide our let god moving.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- core_features_end -->



    <!-- lets_launch_start -->
    <div class="lets_launch launch_bg_1 overlay2">
        <div class="launch_text text-center">
            <h3>Empieza a crear tus encuestas para tu empresa, negocio o cualquier actividad ahora</h3>
            <div class="chat">
                <a class="boxed_btn_green" href="#">
                    <span>Inicia Sesión</span>
                </a>
                <a class="boxed_btn_green2" href="#">
                    <span>Registrate</span>
                </a>
            </div>
        </div>
    </div>
    <!-- lets_launch_end -->
