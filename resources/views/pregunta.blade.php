<body>
	@include('sweet::alert')

	@extends('navbar')
	<!-- bradcam_area_start -->
	<div class="bradcam_area breadcam_bg overlay2">
		<h3>Preguntas</h3>
	</div>
	<!-- bradcam_area_end -->
	<div class="container form-container">
		<h2>Agrega las preguntas</h2>
		<br>
		<form action="{{url('/agregarPre')}}" method="POST" autocomplete="off">
			@csrf
			<div class="form-group row">
				<label class="col-sm-1 col-form-label"><strong>Pregunta:</strong></label>
				<div class="col-sm-11">
					<input type="text" class="form-control" name="titulo_pre" id="titulo_pre">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-1 col-form-label"><strong>Descripción:</strong></label>
				<div class="col-sm-11">
					<input type="text" class="form-control" name="descripcion_pre" id="descripcion_pre">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-1 col-form-label"><strong>Tipo de pregunta:</strong></label>
				<div class="col-sm-6">
					<select class="form-control" name="id_tipo_pregunta" id="id_tipo_pregunta">
						<option value="">--Seleccione opción--</option>
						@foreach($tipo as $tp)
						<option value="{{$tp->id_tipo_pregunta}}">{{$tp->descripcion_tp}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div>
				<input type="submit" class="btn btn-sm btn-primary" value="Agregar">
			</div>
		</form>
	</div>

	<div class="faq_area">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="accordion_heading">
						<h3>Preguntas</h3>
						<br>
						<br>
						<div>
							<a href="{{ url('/home')}}" class="btn btn-primary btn-lg">Finalizar</a>
						</div>
					</div>
					<div id="accordion">
						@foreach($preg as $p)
						<div class="card">
							<div class="card-header" id="headingTwo">
								<h5 class="mb-0">
									<button class="btn btn-link collapsed" data-toggle="collapse"
									data-target="#{{$p->id_pregunta}}" aria-expanded="false" aria-controls="collapseTwo">
									<i class="flaticon-info"></i> {{$p->titulo_pre}}
								</button>
							</h5>
						</div>
						<div id="{{$p->id_pregunta}}" class="collapse" aria-labelledby="headingTwo"
						data-parent="#accordion">
						<div class="card-body">
							<a href="{{ url('/eliminarPre', $p->id_pregunta)}}" class="btn btn-outline-danger">Eliminar</a>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
</div>

