<body>
    @include('sweet::alert')    

    @extends('navbar')
    <!-- bradcam_area_start -->
    <div class="bradcam_area breadcam_bg overlay2">
        <h3>Nueva encuesta</h3>
    </div>
    <!-- bradcam_area_end -->


    <!-- core_features_start -->
    <div class="core_features2 faq_area">
        <div class="container">
            <div class="border-bottm">
                <div class="row">
                    <div class="col-xl-12 col-md-12">
                        <div class="featurest_tabs ">
                            <nav>
                                <div class="nav" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="false">Nueva Encuesta</a>
                                    <a class="nav-item nav-link active show" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="true">Encuestas</a>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div id="accordion">
                                <div class="package_prsing_area">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="single_prising">
                                                    <div class="prising_header">
                                                        <h3>Nueva encuesta</h3>
                                                    </div>
                                                    <div class="middle_content">
                                                        <form method="POST" action="{{ asset('/agregar')}}">
                                                            @csrf
                                                            <div class="list">
                                                                <div class="row">
                                                                    <div class="col-md-11">
                                                                        <label><strong>Titulo</strong></label>
                                                                        <input type="text" name="titulo_en" id="titulo_en" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-11">
                                                                        <label><strong>Descripción</strong></label>
                                                                        <input type="text" name="descripcion_en" id="descripcion_en" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <br>
                                                            </div>
                                                            <div class="start_btn text-center">
                                                                <input type="submit" class="boxed_btn_green" value="Crear">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade active show" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            @foreach($lista as $ls)
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header" id="headingTwoo">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link collapsed" data-toggle="collapse"
                                            data-target="#{{$ls->id_encuesta}}" aria-expanded="false" aria-controls="collapseTwoo">
                                            <i class="flaticon-info"></i> Encuesta. {{$ls->titulo_en}}
                                        </button>
                                    </h5>
                                </div>
                                <div id="{{$ls->id_encuesta}}" class="collapse" aria-labelledby="headingTwoo"
                                    data-parent="#accordion">
                                    <div class="card-body">
                                        <a href="{{ url('/encuesta', $ls->id_encuesta)}}" class="btn btn-outline-info">Ver</a>
                                        <a href="{{ url('/getDatosEnc', $ls->id_encuesta)}}" class="btn btn-outline-success">Modificar</a>
                                        <a href="{{ url('/eliminar', $ls->id_encuesta)}}" class="btn btn-outline-danger">Eliminar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- core_features_end -->
