<body>
    @include('sweet::alert')

    @extends('navbar')

    <!-- bradcam_area_start -->
    <div class="bradcam_area breadcam_bg">
        <h3>Encuesta</h3>
    </div>
    <!-- bradcam_area_end -->

    <!-- core_features_start -->
    <div class="core_features2 faq_area">
        <div class="container container-cont">

            <form action="{{url('/agregarResult')}}" method="POST">
            @csrf

            <!-- Monstrando datos de la encuesta -->
            @foreach($encuesta as $en)
            <h1>{{$en->titulo_en}}</h1>
            <br>
            <h5>{{$en->descripcion_en}}</h5>
            @endforeach

            <!-- Declarando contador -->
            @php
            $n=1;
            @endphp

            @foreach($pregunta as $pre)
            <div class="container1">

                <h5>{{$n}} {{$pre->titulo_pre}}</h5>
                <p>{{$pre->descripcion_pre}}</p>

                <!-- Definiendo el tipo de respuestas segun el id de tipo de pregunta -->
                @php
                if($pre->id_tipo_pregunta == 4){
                @endphp

                <input type="radio" name="si" id="si"> Si
                <br>
                <input type="radio" name="intermedio" id="intermedio"> Intermedio
                <br>
                <input type="radio" name="no" id="no"> No

                @php
                }else{
                @endphp

                <input type="text" name="abierta" id="abierta" class="form-control">

                @php
            }
                @endphp


        </div>

        <!-- Aumentando contador en uno -->
        @php
        $n++;
        @endphp

        @endforeach

        <input type="submit" class="btn btn-primary" value="Finalizar">
    </form>
    </div>
</div>
    <!-- core_features_end -->