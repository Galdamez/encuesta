
<header>
  @php

  if(session()->has('Logueado')){

  @endphp
  <div class="header-area ">
    <div id="sticky-header" class="main-header-area">
      <div class="container-fluid p-0">
        <div class="row align-items-center no-gutters">
          <div class="col-xl-2 col-lg-2">
            <div class="logo-img">
              <a href="index.html">
                <img src="img/logo.png" alt="">
              </a>
            </div>
          </div>
          <div class="col-xl-7 col-lg-7">
                        <div class="main-menu  d-none d-lg-block">
                           <nav>
                               <ul id="navigation">
                                   <li><a class="active" href="#">Inicio</a></li>
                                   <li><a href="../home">Encuestas</a></li>
                                   <!-- <li><a href="#">blog <i class="ti-angle-down"></i></a>
                                       <ul class="submenu">
                                           <li><a href="blog.html">blog</a></li>
                                           <li><a href="single-blog.html">single-blog</a></li>
                                       </ul>
                                   </li>
                                   <li><a href="#">pages <i class="ti-angle-down"></i></a>
                                       <ul class="submenu">
                                           <li><a href="elements.html">elements</a></li>
                                       </ul>
                                   </li> -->
                                   <li><a href="#">Contactos</a></li>
                                   <li><a href="#">Resultados</a></li>
                                   <!-- <li><a href="contact.html">Contact</a></li> -->
                                   <li><a href="#">Configuración <i class="ti-angle-down"></i></a>
                                       <ul class="submenu">
                                           <li><a href="#">Cambiar clave</a></li>
                                       </ul>
                                   </li>
                               </ul>
                           </nav>
                         </div>
                       </div>
                       <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                        <div class="log_chat_area d-flex align-items-center">
                          <a href="{{ url('/logout')}}" class="login">
                            <i class="flaticon-user"></i>
                           Cerrar Sessión
                          </a>
                        </div>
                      </div>
                      <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              @php 

            }else{

            @endphp

            <div class="header-area ">
              <div id="sticky-header" class="main-header-area">
                <div class="container-fluid p-0">
                  <div class="row align-items-center no-gutters">
                    <div class="col-xl-2 col-lg-2">
                      <div class="logo-img">
                        <a href="index.html">
                          <img src="img/logo.png" alt="">
                        </a>
                      </div>
                    </div>
                    <div class="col-xl-7 col-lg-7">
                       <!--  <div class="main-menu  d-none d-lg-block">
                           <nav>
                               <ul id="navigation">
                                   <li><a class="active" href="index.html">home</a></li>
                                   <li><a href="package.html">Package</a></li>
                                   <li><a href="#">blog <i class="ti-angle-down"></i></a>
                                       <ul class="submenu">
                                           <li><a href="blog.html">blog</a></li>
                                           <li><a href="single-blog.html">single-blog</a></li>
                                       </ul>
                                   </li>
                                   <li><a href="#">pages <i class="ti-angle-down"></i></a>
                                       <ul class="submenu">
                                           <li><a href="elements.html">elements</a></li>
                                       </ul>
                                   </li>
                                   <li><a href="Support.html">Support</a></li>
                                   <li><a href="about.html">About</a></li>
                                   <li><a href="contact.html">Contact</a></li>
                               </ul>
                           </nav>
                         </div> -->
                       </div>
                       <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                        <div class="log_chat_area d-flex align-items-center">
                          <a href="#test-form" class="login popup-with-form">
                            <i class="flaticon-user"></i>
                            <span>Inicia Sesión</span>
                          </a>
                        </div>
                      </div>
                      <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              @php
            }
            @endphp

          </header>
