<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Encuesta;
use Illuminate\Support\Facades\DB;

class Home extends Controller
{

    protected $Encuesta;

    public function __construct(Encuesta $Encuesta)
    {
        $this->Encuesta = $Encuesta;
    }

    public function verificarUser(Request $request){

        /*$array = [$request->usuario, $request->clave];*/
        $num = $this->Encuesta->verificar($request->usuario, $request->clave);

        if($num > 0){

            session(['usuario' => $request->usuario, 'clave' => $request->clave, 'Logueado' => true]);
            $user = $request->session()->get('usuario');

            alert()->success('Exito!',$user.' ha iniciado sesión');
            return redirect()->action('Home@inicio');
        }else{

            alert()->error('Ingrese las credenciales correctamente', 'Error!');
            return redirect()->action('Home@index');
        }


    }

    public function logout(Request $request){

        //Removemos todos los datos de la sesión, utilizando el método flush
        $request->session()->flush();
        alert()->info('Exito!','Se cerro sesión correctamente');
        return redirect()->action('Home@index');

    }

    public function index(Request $request){

        if ($request->session()->has('Logueado')) {

            return redirect()->action('Home@inicio');

        }else{

            echo view('template/header');
            echo view('index');
            echo view('template/footer');

        }
    }

    public function inicio(Request $request){

        if ($request->session()->has('Logueado')) {

            $lista = $this->Encuesta->getEncuestas();

            echo view('template/header');
            echo view('home', compact('lista'));
            echo view('template/footer');
        }else{

            return redirect()->action('Home@index');

        }

    }

    public function pregunta(Request $request){

        if ($request->session()->has('Logueado')) {

            $data['tipo'] = $this->Encuesta->getTipoPreg();
            $data['preg'] = $this->Encuesta->getPregunta();

            echo view('template/header');
            echo view('pregunta', $data);
            echo view('template/footer');

        }else{

            return redirect()->action('Home@index');

        }

    }

    public function agregarEncuesta(Request $request){

        $array = [$request->titulo_en, $request->descripcion_en];
        $this->Encuesta->setEncuesta($array);
        alert()->success('Agrege las preguntas','Se agrego nueva encuesta')->persistent('Continuar');
        return redirect()->action('Home@pregunta');
    }

    public function eliminarEncu($id){

        DB::table('encuestas')->where('id_encuesta', '=', $id)->delete();
        return redirect()->action('Home@inicio');
    }

    public function agregarPregunta(Request $request){

        $array = [$request->id_tipo_pregunta, $request->titulo_pre, $request->descripcion_pre];
        $this->Encuesta->setPregunta($array);
        return redirect()->action('Home@pregunta');
    }

    public function eliminarPre($id){
        $this->Encuesta->eliminarPre($id);
        return redirect()->action('Home@pregunta');
    }

   // Creamos un método para atender la peticion llamado getEncuesta
    public function getDatosEnc($id){


        $data['e'] = DB::table('encuestas as e')->where('e.id_encuesta',$id)->get();

        $data['p'] = DB::table('preguntas as e')->join('encuestas as p', 'p.id_encuesta','=','e.id_encuesta')->where('e.id_encuesta',$id)->get();

        // retornamos la vista con los datos
        echo view('template/header');
        echo view('encuestaAct',$data);
        echo view('template/footer');

    }

    public function modificarEnc(Request $request){

        $datos = [$request->titulo_pre, $request->descripcion_pre, $request->id_encuesta];

        $array = $this->Encuesta->modificarEnc($datos);
        return response()->json($array);
    }

    public function modificarPre(Request $request){

        $datos = [$request->pregunta, $request->id_preg];

        $array = $this->Encuesta->modificarPre($datos);
        return response()->json($array);
    }

    public function encuesta($id){

        $data['pregunta'] = $this->Encuesta->getEncuesta($id);
        $data['encuesta'] = $this->Encuesta->getSurvey($id);

        echo view('template/header');
        echo view('encuesta', $data);
        echo view('template/footer');
    }


}
