<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Encuesta extends Model
{   

  public function verificar($usuario,$clave){

    $user = \DB::table('usuarios')->where([
      ['usuario', '=', $usuario],
      ['clave', '=', $clave],
    ])->get();

    return $user->count();
  }

  public function getEncuestas(){

   return \DB::table('encuestas')->select('encuestas.titulo_en', 'encuestas.id_encuesta')->get();
 }

 public function setEncuesta($array){

   return \DB::select('CALL sp_insert_encuesta(?,?)', $array);
 }

 public function getTipoPreg(){

   return \DB::table('tipo_pregunta')->get();
 }

 public function setPregunta($array){

  return \DB::select('CALL sp_insert_pregunta(?,?,?)', $array);
}

public function getPregunta(){

  return \DB::select('CALL sp_get_preg()');
}

public function eliminarPre($id){

  return \DB::table('preguntas')->where('id_pregunta', '=', $id)->delete();
}

public function getDatosEnc($id){

  return \DB::select('CALL sp_get_datos(?)', $id);
}

public function modificarEnc($datos){

  return \DB::select('CALL sp_update_encuesta(?,?,?)',$datos);
}

public function modificarPre($datos){

  return \DB::select('CALL sp_update_preg(?,?)',$datos);
}

public function getEncuesta($id){

  return \DB::table('preguntas')->where('id_encuesta',$id)->get();
}

public function getSurvey($id){

  return \DB::table('encuestas')->where('id_encuesta',$id)->get();
}
}
